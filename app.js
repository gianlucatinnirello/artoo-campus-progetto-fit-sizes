angular.module('artoo', [
  'ngMaterial',
  'ui.router'
])
  .config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('blue-grey')
      .accentPalette('cyan')
      .warnPalette('red')
      .backgroundPalette('grey');
  });
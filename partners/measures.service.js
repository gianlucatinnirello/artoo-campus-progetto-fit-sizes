angular.module('artoo').factory('Measures', function () {
  //misurazione per tipologia di capo
  var sizes = {
      shoulder: {
        s: {
            min: 10,
            max: 0
            },
        m: {
            min: 10,
            max: 0
            },
        l: {
            min: 10,
            max: 0
            }
      },
      normal_waist: {
        s: {
            min: 10,
            max: 0
            },
        m: {
            min: 10,
            max: 0
            },
        l: {
            min: 10,
            max: 0
            }
      },
  };
  
  var getAllSizes = function () {
    return sizes;
  }
 
  var getSize = function (measures, size) {
    return sizes[measures][size];
  }; 
 
  var setSize = function (measures, size, valuemin, valuemax) {
    sizes[measures][size].min = valuemin;
    sizes[measures][size].max = valuemax;
  };
  
  return {
    getAllSizes: getAllSizes,
    getSize: getSize,
    setSize: setSize
  };
});
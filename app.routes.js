angular.module('artoo')
  .config(function ($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/');
  
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'home/home.template.html'
      })
      
      .state('partners', {
        abstract: true,
        url: '/partners',
        templateUrl: 'partners/partners.template.html'
      })
      
      .state('partners.info', {
        url: '/info/:id',
        templateUrl: '/partners/info/info.partners.template.html',
        controller: 'PartnersInfoCtrl',
        resolve: {
          partner: function ($stateParams, $log, Partners) {
            return Partners.getPartnerById($stateParams.id);
          }
        }
      })
      
      .state('partners.measures', {
        url: '/measures',
        templateUrl: '/partners/measures/measures.partners.template.html',
        controller: 'PartnersMeasuresCtrl'
      })
      
      .state('partners.list', {
        url: '/list',
        templateUrl: '/partners/list/list.partners.template.html',
        controller: 'PartnersListCtrl'
      });
    
  });